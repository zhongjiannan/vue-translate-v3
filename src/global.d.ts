import type {ElectronAPI} from "@electron-toolkit/preload"
import type {IncomingHttpHeaders} from "http"

export {}

declare global{

	namespace Electron{
		interface App extends NodeJS.EventEmitter {
			on<E extends keyof AppEvents>(event: E, listener: AppEvents[E], ...args: any[]): this;
		}
	}

	// 目录信息数据结构
	interface IDirectory {
		name: string,
		path: string,
		relative: string,
		type: string,
		children?: Array<IDirectory>
	}

	// 扩展请求配置
	interface IRequestOptions extends RequestInit{
		responseType?: XMLHttpRequestResponseType
	}

	// 响应数据
	interface IResponse<T>{
		headers: IncomingHttpHeaders,
		statusCode?: number,
		statusMessage?: string,
		data?: T
	}

	// 翻译响应
	interface TranslateResponse {
		// 响应状态
		status: 'success' | 'error',
		// 当前语言
		from: string,
		// 目标语言
		to: string,
		// 翻译原文
		keywords: string
		// 翻译结果
		data: string
	}

	// 全局API注入
	interface Bridge {
		electron: ElectronAPI,
		readDirectory: (directory:string, base?:string, exclude?:Array<string>) => Promise<Array<IDirectory>>,
		readFile: (path: string) => Promise<string>
		readDirectorySync: (directory:string, base?:string, exclude?:Array<string>) => Array<IDirectory>,
		md5: (str: string) => string,
		writeFile:(path: string, content: string) => Promise<any>
		writeFileSync:(path: string, content: string) => void
	}

	interface PacketData {
		path: string,
		data: Record<string, string>
	}

	interface TranslateTaskResult{
		locale: string,
		data: Array<PacketData>
	}

	interface CommonConfig {
		exclude: Array<string>
	}

	interface BaiduAuthorization{
		secret: string,
		appid: string
	}

	interface ApplicationStoreConfig {
		common: CommonConfig,
		baidu: BaiduAuthorization
	}

	interface ApplicationYamlConfig {
		install?: number,
		cwd: string,
		config: ApplicationStoreConfig
	}

	interface Window{
		bridge: Bridge
	}
}
