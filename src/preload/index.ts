import {contextBridge} from 'electron'
import {electronAPI} from '@electron-toolkit/preload'
import * as utils from './utils'

if (process.contextIsolated) {
	try {
		contextBridge.exposeInMainWorld('bridge', { electron: electronAPI, ...utils })
	} catch (error) {
		console.error(error)
	}
} else {
	// @ts-ignore (define in src/global.d.ts)
	window.bridge = { electron: electronAPI, ...utils }
}
