import Store from 'electron-store'

const config: ApplicationStoreConfig = {
    common: {
        exclude: ['node_modules'],
    },
    baidu: {
        appid: '',
        secret: ''
    }
}


export default function initPreferences(store: Store<ApplicationYamlConfig>){
    if(store.get('install')) return
    store.set('install', Date.now())
    store.set('config', config)
}