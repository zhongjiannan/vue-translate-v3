import { app } from 'electron'
import Store from 'electron-store'
import yaml from 'js-yaml'

export const installedPath = app.getAppPath().split("\\resources\\app.asar")[0]

const store = new Store<ApplicationYamlConfig>({
	cwd: installedPath,
	name: 'application',
	fileExtension: 'yml',
	serialize: yaml.dump,
	deserialize: yaml.load
})

store.set('cwd', installedPath)

export default store