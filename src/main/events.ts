import { ipcMain, dialog } from "electron";
import store from "./store";

// 打开目录
ipcMain.handle('request-open-directory', () => {
     return dialog.showOpenDialog({ properties: ['openDirectory'] })
})

ipcMain.handle('get-install-path', _ => {
	return store.get('cwd')
});

ipcMain.handle('get-config', _ => {
	return store.get('config')
});

ipcMain.handle('set-config', (_:Electron.IpcMainInvokeEvent, payload:string) => {
	return store.set('config', JSON.parse(payload))
});
