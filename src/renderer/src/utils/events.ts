import { OpenDialogReturnValue } from 'electron'
const { ipcRenderer } = window.bridge.electron


export function openDirectory(): Promise<OpenDialogReturnValue>{
    return ipcRenderer.invoke('request-open-directory')
}

export function getStoreConfig(){
    return ipcRenderer.invoke('get-config') as Promise<ApplicationStoreConfig>
}


export function setStoreConfig(config: ApplicationStoreConfig){
    return ipcRenderer.invoke('set-config', JSON.stringify(config))
}