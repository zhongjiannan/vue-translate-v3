export * from './http'
export * from './events'
export * from './locale'

/**
 * 获取文本种匹配的内容
 * @param {string} text
 * @param {string|RegExp} regex
 * @return Array<string>
 */
export function getMatchContent(text: string, regex: string | RegExp): Array<string> {
	if (!(regex instanceof RegExp)) regex = new RegExp(regex, 'g')
	let result: RegExpExecArray | null;
	const data: Array<string> = []
	while ((result = regex.exec(text)) !== null) {
		data.push(result[1])
	}
	return data
}

/**
 * 深拷贝
 * @param { T } data 数据源
 * @returns { T } 
 */
export function deepClone<T>(data:T): T{
	let d: T;
	try{
		d = structuredClone(data)
	}catch (_){
		d = JSON.parse(JSON.stringify(data))
	}
	return d
}

/**
 * 首字母大写
 * @param { string } text 输入字符
 */
export function firstUppercase(text:string){
	return text.slice(0,1).toUpperCase() + text.slice(1)
}


/**
 * 首字母小写
 * @param { string } text 输入字符
 */
export function firstLowercase(text:string){
	return text.slice(0,1).toLowerCase() + text.slice(1)
}

/**
 * 异步休眠
 * @param timeout 时间
 * @returns { Promise<void> } 
 */
export function sleep(timeout:number){
	return new Promise<void>((resolve) => {
		setTimeout(() => {
			resolve(undefined)
		}, timeout);
	})
}

/**
 * 同步休眠
 * @param timeout 时间
 */
export function sleepSync(timeout:number){
	const start = Date.now()
	while(true){
		const now = Date.now()
		if((start + timeout) > now) break;
	}
}