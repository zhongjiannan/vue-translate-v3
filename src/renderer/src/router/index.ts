import {createRouter, createWebHashHistory} from 'vue-router'
import routes from "./routes";

const router = createRouter({
	history: createWebHashHistory(),
	routes: [
		{
			path: '/',
			component: () => import('@renderer/layout/MainLayout.vue'),
			children: routes
		}
	]
})

export default router
