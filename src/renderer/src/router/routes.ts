import {RouteRecordRaw} from "vue-router";

const routes: Array<RouteRecordRaw> = [
	{
		path: '',
		component: () => import('@renderer/pages/Home/Index.vue'),
		meta: { title: '首页' }
	},
	{
		path: 'translate',
		component: () => import('@renderer/pages/Translate/Index.vue'),
		meta: { title: '项目翻译' }
	},
	{
		path: 'simple',
		component: () => import('@renderer/pages/Simple/Index.vue'),
		meta: { title: '简体识别' }
	},
	{
		path: 'setting',
		component: () => import('@renderer/pages/Setting/Index.vue'),
		meta: { title: '设置' }
	}
]

export default routes
