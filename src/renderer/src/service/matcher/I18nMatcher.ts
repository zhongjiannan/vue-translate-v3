import { Matcher } from "..";

export default class I18nMather implements Matcher {
    public readonly regex = /[^\\w]t\([''|\"](.*?)[''|\”\"]\)/g;
    public readonly name = "Vue-I18n"
    public readonly id = crypto.randomUUID()
    
    match(content: string){
        let result: RegExpExecArray | null;
        const data: Array<string> = []
        while ((result = this.regex.exec(content)) !== null) {
            data.push(result[1])
        }
        return data
    }

    replace(template: string, data: Record<string, string>){
        return template.replace(this.regex, (match, p1) => {
            const replacement = data[p1] || p1;
            return match.replace(p1, replacement);
        });
    }
}