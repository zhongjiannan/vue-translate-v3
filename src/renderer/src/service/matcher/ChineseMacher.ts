import { Matcher } from "..";

export default class ChineseMatcher implements Matcher {
    public readonly regex = /(?:\/\*(?:[\s\S]*?)\*\/|\/\/.*|<!--[\s\S]*?-->|"""\s*[\s\S]*?\s*"""|'''\s*[\s\S]*?\s*''')|([\u4e00-\u9fa5]+)/g;
    public readonly name = "中文字符";
    public readonly id = crypto.randomUUID();

    match(content: string) {
        let result: Array<string> = [];
        let match;

        while ((match = this.regex.exec(content)) !== null) {
            if (match[1]) result.push(match[1]);
        }

        return result;
    }

    replace(template: string, data: Record<string, string>){
        return template.replace(this.regex, (match) => data[match] || match);
    }
}