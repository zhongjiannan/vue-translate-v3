import { Exporter } from "..";

export default class JsonExporter implements Exporter{
    public readonly id = crypto.randomUUID()
    public readonly name = 'JSON'
    private static readonly SUFFIX = '.json'

    export(data: Array<TranslateTaskResult>){
        const transferMaps:Record<string, string> = {}
        data.forEach(({ locale, data }) => {
            transferMaps[locale + JsonExporter.SUFFIX] = this.createCode(data)
        })
        return transferMaps
    }

    createCode(data: Array<PacketData>){
        let content = '{\n'
        data.forEach(item => {
            for(let key in item.data){
                content+= '\t' + JSON.stringify(key) + ':' + JSON.stringify(item.data[key]) + ',' + '\n'
            }
        })
        content = content.slice(0, -2)
        content += '\n}'
        return content
    }
}