import { Exporter } from "..";

export default class TxtExporter implements Exporter{
    public readonly id = crypto.randomUUID()
    public readonly name = '文本文件'
    private static readonly SUFFIX = '.txt'

    export(data: Array<TranslateTaskResult>){
        const transferMaps:Record<string, string> = {}
        data.forEach(({ locale, data }) => {
            transferMaps[locale + TxtExporter.SUFFIX] = this.createCode(data)
        })
        return transferMaps
    }

    createCode(data: Array<PacketData>){
        let content = ''
        data.forEach(item => {
            for(let key in item.data){
                content += key + ' = ' + item.data[key] + '\n'
            }
        })
        content = content.slice(0, -1)
        return content
    }
}