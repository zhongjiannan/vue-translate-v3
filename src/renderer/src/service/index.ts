import { firstLowercase } from '@renderer/utils'

export interface Translate{
	readonly id: string,
	readonly name: string,
	readonly delay: number
	translate: (keywords: string, to:string, from: string) => Promise<TranslateResponse>
}

export interface Matcher{
    readonly regex: RegExp,
    readonly id: string,
    readonly name: string,
    match: (content: string) => Array<string>
    replace: (template: string, data: Record<string, string>) => string
}

export interface Exporter{
    readonly id: string,
    readonly name: string,
    export: (data: Array<TranslateTaskResult>) => Record<string, string>
}

export enum StandardLocale {
    'zh-CN' = '简体中文',
    'zh-TW' = '繁体中文',
    'en-US' = '英语'
}

type ModuleType<T> = {
    default: T;
}

type TranslateCtor = new () => Translate
type MatcherCtor = new () => Matcher
type ExporterCtor = new () => Exporter

// 批量导出Translate模块
export const translaters = Object.fromEntries(Object.values(import.meta.glob<true, string, ModuleType<TranslateCtor>>("./translater/*.ts", { eager: true })).map(item => {
    const ctor = item.default
    return [firstLowercase(ctor.name), new ctor()]
}))


// 批量导出Matcher模块
export const matchers = Object.fromEntries(Object.values(import.meta.glob<true, string, ModuleType<MatcherCtor>>("./matcher/*.ts", { eager: true })).map(item => {
    const ctor = item.default
    return [firstLowercase(ctor.name), new ctor()]
}))


// 批量导出Exporter模块
export const exporters = Object.fromEntries(Object.values(import.meta.glob<true, string, ModuleType<ExporterCtor>>("./exporter/*.ts", { eager: true })).map(item => {
    const ctor = item.default
    return [firstLowercase(ctor.name), new ctor()]
}))