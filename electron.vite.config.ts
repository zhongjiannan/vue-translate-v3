import {resolve} from 'path'
import {defineConfig, externalizeDepsPlugin, bytecodePlugin} from 'electron-vite'
import vue from '@vitejs/plugin-vue'
import AutoImport from 'unplugin-auto-import/vite'
import Components from 'unplugin-vue-components/vite'
import { ElementPlusResolver } from 'unplugin-vue-components/resolvers'

export default defineConfig({
	main: {
		plugins: [
			externalizeDepsPlugin(),
			bytecodePlugin()
		]
	},
	preload: {
		plugins: [
			externalizeDepsPlugin(),
			bytecodePlugin()
		]
	},
	renderer: {
		resolve: {
			alias: {
				'@renderer': resolve('src/renderer/src')
			}
		},
		plugins: [
			vue(),
			AutoImport({
				resolvers: [ElementPlusResolver()],
				imports: ["vue","vue-router"],
				dts: resolve('src/renderer/src/types/auto-imports.d.ts')
			}),
			Components({
				resolvers: [ElementPlusResolver()],
				dts: resolve('src/renderer/src/types/component.d.ts')
			})
		],
		build: {
			rollupOptions: {
				output: {
					assetFileNames: assetInfo => {
						const name = assetInfo.name!
						const info = name!.split('.')
						let ext = info[info.length - 1]
						if (/\.(mp4|webm|ogg|mp3|wav|flac|aac)(\?.*)?$/i.test(name)) {
							ext = 'media'
						} else if (/\.(png|jpe?g|gif|svg)(\?.*)?$/.test(name)) {
							ext = 'img'
						} else if (/\.(woff2?|eot|ttf|otf)(\?.*)?$/i.test(name)) {
							ext = 'fonts'
						}
						return `${ext}/[hash][extname]`
					},
					chunkFileNames: 'js/[hash:8].js',
					entryFileNames: 'js/[hash:8].js',
					manualChunks: () => {
						return 'vendor'
					}
				}
			},
			minify: true
		}
	}
})
